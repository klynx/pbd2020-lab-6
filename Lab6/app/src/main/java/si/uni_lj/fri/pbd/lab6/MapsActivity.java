package si.uni_lj.fri.pbd.lab6;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.app.PendingIntent;
import android.content.PeriodicSync;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.snackbar.Snackbar;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, ActivityCompat.OnRequestPermissionsResultCallback {

    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private int REQUEST_ID_LOCATION_PERMISSIONS = 0;
    private String TAG = "MapsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Get location stuff
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        showLastKnownLocation();
    }

    private boolean rationaleRequired() {
        return ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION);
    }

    private boolean hasPermissions() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private void askPermissions () {
        ActivityCompat.requestPermissions(MapsActivity.this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_ID_LOCATION_PERMISSIONS);
    }

    public void showLastKnownLocation() {
        Log.i(TAG, "Method Called");
        if (!hasPermissions()) {
            if (rationaleRequired()) {
                Log.i(TAG, "displaying rationale and asking permissions");
                Snackbar.make(findViewById(R.id.layout_map), R.string.permission_location_rationale, Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.ok, (view) -> askPermissions())
                        .show();
            }
            else
                Log.i(TAG, "asking for permissions first time");
                askPermissions();
            return;
        }

        mFusedLocationProviderClient.getLastLocation()
                .addOnSuccessListener(location -> {
                    Log.d(TAG, "got Latitude " + location.getLatitude());
                    Log.d(TAG, "got longitude " + location.getLongitude());
                    LatLng positionLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                    mMap.addMarker(new MarkerOptions()
                        .position(positionLatLng)
                            .title(getString(R.string.update_time, DateFormat.getTimeInstance().format(new Date(location.getTime()))))
                    );

                    TextView latitudeText = findViewById(R.id.latitude);
                    TextView longitudeText = findViewById(R.id.longitude);
                    latitudeText.setText(getString(R.string.latitudeText, location.getLatitude()));
                    longitudeText.setText(getString(R.string.longitudeText, location.getLongitude()));
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(positionLatLng, 8);
                    mMap.animateCamera(cameraUpdate);
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResult) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResult);
        if (requestCode == REQUEST_ID_LOCATION_PERMISSIONS) {
            if (grantResult.length > 0 && Arrays.stream(grantResult).allMatch(result -> result == PackageManager.PERMISSION_GRANTED))
                showLastKnownLocation();
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    public void showLastKnownLocation(View view) {
        showLastKnownLocation();
    }
}
